﻿using System;
using System.Web.Mvc;
using NServiceBus;
using System.Security.Principal;
using System.Threading;
using Messages;
using System.Threading.Tasks;

public class SendAsyncController : AsyncController
{
    private IBus bus;
    private IAsyncResult asyncResult;

    public SendAsyncController(IBus bus)
    {
        this.bus = bus;
    }

    [HttpGet]
    public ActionResult Index()
    {
        ViewBag.Title = "SendAsync";
        return View("Index");
    }

    [HttpPost]
    [AsyncTimeout(50000)]
    public async Task<string> IndexAsync(string textField)
    {
        int number;
        int.TryParse(textField, out number);

        var identity = new GenericIdentity("userA");
        Thread.CurrentPrincipal = new GenericPrincipal(identity, null);

        Command command = new Command
        {
            Id = number
        };

        var task = new Task<string>(() =>
        {
            var answer = (ResponseMessage)((CompletionResult)asyncResult.AsyncState).Messages[0];
            return answer.Property;
        });

        bus.Send("Samples.Mvc.Server", command)
            .Register(ar =>
                {
                    asyncResult = ar;
                    task.Start();
                },
                this);

        return await task;
    }
}