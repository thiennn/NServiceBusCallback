﻿using NServiceBus;

namespace Messages
{
    public class ResponseMessage : IMessage
    {
        public string Property { get; set; }
    }
}
