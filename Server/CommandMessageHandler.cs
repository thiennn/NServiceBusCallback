﻿using System;
using NServiceBus;
using Messages;

public class CommandMessageHandler : IHandleMessages<Command>
{
    IBus bus;

    public CommandMessageHandler(IBus bus)
    {
        this.bus = bus;
    }

    public void Handle(Command message)
    {
        Console.WriteLine("Hello from CommandMessageHandler");
        Console.WriteLine("Current User: " + System.Threading.Thread.CurrentPrincipal.Identity.Name);

        if (message.Id%2 == 0)
        {
            Console.WriteLine("Returning Fail");
            bus.Reply(new ResponseMessage
            {
                Property = "Returning Fail"
            });
        }
        else
        {
            Console.WriteLine("Returning None");
            bus.Reply(new ResponseMessage
            {
                Property = "Returning None"
            });
        }
    }
}
